## MicroTik
### Dispositivo
- Altera o nome do dispositivo
      
      system identity set name=<nome_do_dispositivo>

### Traceroute
    tool traceroute <ip>

### Interface
- Mostrar interfaces de rede
      
      interface print

    - Lista as interfaces com mais detalhes como o MAC por exemplo
          
          interface print details 

- Lista as interfaces que estão conectadas e a quem ela esta conectada
      
      ip neighbor print

### IP
- Adicionar ip a uma interface
      
      ip address add address=<ip> interface=<interface>

- Configurar o ip de uma interface
      
      ip address set address=<ip> interface=<interface>

### Rota estática
    ip route add dst-address=<ip_destino> gateway=<ip_de_quem_sabe>

### Backup
- Mostra a configuração do microtik que você alterou (mas em texto)
        
      export terse compact

- Salva em um arquivo os comandos utilizados 
        
      export file=<nome do arquivo>

- Mostra todas exports e backup existentes
        
      file print

- Importa o arquivo com os comandos
        
      import filename <filename>

- Cria um backup

      system backup save name=<filename>

- Reseta o microtik
        
      system reset-configuration no-defaults=yes (reseta e não coloca as configurações padrões)

- Usa o backup informado
        
      system load name=<filename>

### OSPF
- loopback: Uma bridge sem ip
        
      interface bridge add name=<name>

- Mostra todas as instâncias
     
      routing ospf instance print

- Configura instância (Executa no mk2, assim ele manda todas as rotas do mk2 para o mk1)

      routing ospf instance set router-id=<ip loopback> redistribute-connected=as-type-1 redistribute-static=as-type-1

    - `router-id=<ip loopback>` Configura o id da instância (normalmente o ip da loopback)
    - `redistribute-connected=as-type-1` Vai destribuir todas as rotas que estão conectadas a máquina(não as estáticas), e vai colocar a distância como 110
    - `redistribute-static=as-type-1` Vai destribuir todas as rotas estáticas, e vai colocar a distância como 110 

- Mostra as networks
        
      routing ospf network print

- Configura uma network
        
      routing ospf network set network=<ip network da rede> area=<>

- Mostra as conexões (O state Full quer dizer que ele enlaçou)
        
      routing ospf neighbor print      

- Mostra as redes que o ospf criou mas dizendo quem criou ela
        
      routing ospf lsa print
