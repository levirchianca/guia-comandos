## OLT FIBERHOME
- Autotiza onu

      set whitelist phy_address <onu> password null action add slot <slot> link <pon> onu <indice> type <model>

- Desautoriza onu

      set whitelist phy_address <onu> password null action delete
      
- Mostra quais onus fora autorizadas(mesmo as que estão down).

      show authorization slot <slot> link <pon>

- Mostra somente as onus que estão up naquele slot e pon

      show online slot <slot> link <pon>

- Mostra as onus para autorizar

      show unauth discovery

- Impede que o terminal não mostre resultados grandes

      terminal lenght 0

- Mostra todas as vlans que estão configuradas na OLT

      vlan show service vlan
      
- 
      epononu qinq show epon slot <> pon <> onu <> port <> service 
